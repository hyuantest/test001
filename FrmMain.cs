﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cognex.VisionPro;
using Cognex.VisionPro.ToolBlock;
using SocketConfig;
using System.Net;
using LoggerConfig;
using System.Threading;
using VisionProcess;
using Cognex.VisionPro.ImageFile;
using System.IO.Ports;
using System.Diagnostics;
using SerialPortConfig;
using Cognex.VisionPro.Display;
using csIOC0640;
using System.IO;
using Port;

namespace VisionPro
{
    public partial class FrmMain : Form
    {

        #region 全局变量

        string SolutionPath;  //方案路径     
        string mAcqCCD1Path;    //取像vpp存放路径
        string mAcqCCD2Path;    //取像vpp存放路径
        string mAcqCCD3Path;    //取像vpp存放路径     
        string mCheckCCD1Path;  //FD1检测Vpp
        string mCheckCCD2Path;  //FD1检测Vpp
        string mCheckCCD3Path;  //BM检测Vpp
        string mCalibrationVppPath;//相机1校准vpp
        string mCalibrationVppPath1;//相机2校准vpp
        string mCalibrationVppPath2;//相机3校准vpp
        string mGetRotationVppPath;//机器人旋转中心vpp
       
        string mProductCode;  //产品条码

        CogAcqFifoTool mAcqCCD1; //FD1相机
        CogAcqFifoTool mAcqCCD2; //FD2相机
        CogAcqFifoTool mAcqCCD3; //BM相机

        CogToolBlock mCheckCCD1; //FD1检测
        CogToolBlock mCheckCCD2; //FD2检测
        CogToolBlock mCheckCCD3; //BM检测

        CogToolBlock mCalibrationTool;//标定1 
        CogToolBlock mCalibrationTool1;//标定2
        CogToolBlock mCalibrationTool2;//标定3  
        CogToolBlock mGetRotationTool; //旋转中心
        List<CogAcqFifoTool> mAcqFifoTools=new List<CogAcqFifoTool> ();
        List <CogToolBlock>  mCheckCCDs=new List<CogToolBlock> ();
        List<CogToolBlock> mCalibrationTools = new List<CogToolBlock>();
        List <string > mAcqPaths=new List<string> ();
        List<string> mCheckPaths = new List<string>();
        List<string> mCalibrationPaths = new List<string>();

        //SocketHelper.TcpServer mTcpServer;//tcp服务器
        //SocketHelper.TcpClients mTcpClient;//tcp客户端

        //IPAddress mServerIp = IPAddress.Parse("127.0.0.1");   //服务器IP
        //IPAddress mClientIp = IPAddress.Parse("127.0.0.1");   //客户端IP


        //private int mPort = 5001;//网口通信端口

        //IPEndPoint mUsedIP;  //已绑定的正在使用的客户端ip及本地端口信息
        Logger mlogger = new Logger();//LOG操作类
        long TextLine;//监控面板显示行数     
        ImageProcess mProcess=new ImageProcess();//图像处理类
        CogImage8Grey mImage=new CogImage8Grey();
        CogImage24PlanarColor mImage1=new CogImage24PlanarColor();
        CogImageFile mImageFile = new CogImageFile();
        int mCaliStation = 0;//标定标识，0默认为不启用标定，1为启用标定 
        string ConnectModel;//通讯模式
        int ReconnectNums;//客户端重连次数
        Thread ReconnectThread;//监听客户端是否掉线线程
        Thread ReadIOThread;   //读取IO卡线程
                               // Thread WriteIOThread;   //读取IO卡线程
        Stopwatch watch; //获取运行时间
        //string ReceiveData;//通信接收数据 
        ParamDatas mParam = new ParamDatas();
        Fileoperation mOperation = new Fileoperation();
        AutoSizeForm mAsc = new AutoSizeForm();
        string SNcode = "";//  产品条码
        string LoadPath;
        string mProductName;//当前产品名
        int productNum;//当前产品序号
        #endregion
        public FrmMain()
        {

            SolutionPath = ParamDatas.SolutionPath; //当前方案路径       
            mProductName = mOperation.GetIni("方案设置", "当前产品", strpath);
            productNum = Convert.ToInt32(mProductName.Substring(mProductName.Length - 1, 1));

            LoadPath = SolutionPath + "\\" + mProductName;//当前加载路径
            InitializeComponent();
         
            if (LoadPath !=null )
            {
                UpdateVpp(LoadPath);
                AutoSizeScreen();
                productPath = LoadPath;              
                UpadteParam(LoadPath);
                ThreadPool.QueueUserWorkItem(new WaitCallback(LoadVpp), LoadPath); //初始化vpp     
            }              
        }
        FrmParamData FrmData ;
        /// <summary>
        /// 接收来自参数界面消息
        /// </summary>
        /// <param name="msg"></param>
         private void ReceiveParamMsg( string msg)
        {
           if(msg !=null )
                UpadteParam(productPath);
        }
        /// <summary>
        /// 参数更新
        /// </summary>
        /// <param name="path"></param>
        private void UpadteParam(string path)
        {
            string mpath = path + "\\config.ini";
            try
            {
                CCD1CaliX = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD1基准X", mpath));
                CCD1CaliY = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD1基准Y", mpath));
                CCD1CaliAngle = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD1基准角度", mpath));
                CCD2CaliX = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD2基准X", mpath));
                CCD2CaliY = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD2基准Y", mpath));
                CCD2CaliAngle = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD2基准角度", mpath));
                CCD3CaliX = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD3基准X", mpath));
                CCD3CaliY = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD3基准Y", mpath));
                CCD3CaliAngle = Convert.ToDouble(mOperation.GetIni("参数设置", "CCD3基准角度", mpath));

            }
           
            catch (Exception ex)
            {
                Monitor("参数导入异常！"+ex.Message ,3);
            }
        }
        /// <summary>
        /// 复制vpp到指定路径
        /// </summary>
        /// <param name="copyPath">原来路径</param>
        /// <param name="pastePath">新路径</param>
        private void copyVpp(string copyPath,string pastePath)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(copyPath);
                FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //获取目录下（不包含子目录）的文件和子目录
                foreach (FileSystemInfo i in fileinfo)
                {
                    if (i is DirectoryInfo)     //判断是否文件夹
                    {
                        if (!Directory.Exists(pastePath + "\\" + i.Name))
                        {
                            Directory.CreateDirectory(pastePath + "\\" + i.Name);   //目标目录下不存在此文件夹即创建子文件夹
                        }
                        copyVpp(i.FullName, pastePath + "\\" + i.Name);    //递归调用复制子文件夹
                    }
                    else
                    {
                        File.Copy(i.FullName, pastePath + "\\" + i.Name, true);      //不是文件夹即复制文件，true表示可以覆盖同名文件
                    }
                }
            }
            catch (Exception e)
            {
                Monitor("新建产品失败！" + e.Message, 3);
            }
        }
        /// <summary>
        ///更新产品Vpp路径
        /// </summary>
        /// <param name="path"></param>
        private void UpdateVpp(string path)
        {
                      
            string CheckToolPath = path + @"\CheckTools";
            string AcqToolPath = path + @"\AcqfifoTools";
            string CaliToolPath = path + @"\CalibrationTools";
            try
            {
                if (mCheckPaths .Count >0)
                    return;
                //获取检测工具文件夹下所有文件
                DirectoryInfo CheckFolder = new DirectoryInfo(CheckToolPath);
                foreach (FileInfo fileName in CheckFolder.GetFiles())
                {
                    string CheckvppName = CheckToolPath + "\\" + fileName.Name;
                    mCheckPaths.Add(CheckvppName);
                }
                //获取取像工具文件夹下所有文件
                DirectoryInfo AcqFolder = new DirectoryInfo(AcqToolPath);
                foreach (FileInfo fileName in AcqFolder.GetFiles())
                {
                    string AcqvppName = AcqToolPath + "\\" + fileName.Name;
                    mAcqPaths.Add(AcqvppName);
                }
                //获取标定工具文件夹下所有文件
                DirectoryInfo CaliFolder = new DirectoryInfo(CaliToolPath);
                foreach (FileInfo fileName in CaliFolder.GetFiles())
                {
                    string CalivppName = CaliToolPath + "\\" + fileName.Name;
                    mCalibrationPaths.Add(CalivppName);
                }
            }
            catch (Exception ex)
            {
                Monitor("程序配置失败" + ex.Message, 3);
            }


        }
        /// <summary>
        /// 保存当前vpp
        /// </summary>
        private void SaveVpp()
        {
           //保存取像vpp
            int i=0;
            foreach ( CogAcqFifoTool mAcq in mAcqFifoTools)
            {

                CogSerializer.SaveObjectToFile(mAcq, mAcqPaths [i]);
                i++;

            }
            //保存检测vpp
            int j = 0;
            foreach( CogToolBlock checkTool in mCheckCCDs)
            {
                CogSerializer.SaveObjectToFile(checkTool, mCheckPaths [j]);
                j++;
            }
            //保存标定vpp
            int k = 0;
            foreach (CogToolBlock caliTool in mCalibrationTools)
            {
                CogSerializer.SaveObjectToFile(caliTool, mCalibrationPaths[k]);
                k++;
            }
        }
       /// <summary>
       /// 初始化通信端口
       /// </summary>
        void InitializePort()
        {
            BindPort(true);
            TcpCollection.OpenAllPort();         
        }
        /// <summary>
        /// 绑定端口 
        /// </summary>
        /// <param name="Enabled"></param>
        void BindPort(bool Enabled)
        {
            if (Enabled)
            {
                foreach (var p in TcpCollection.Instance)
                {
                    p.ReceiveDataAction = ReceiveData;
                    p.ConnectStatsAction = ConnectStats;
                    p.ShowMessageAction = ShowMessage;
                }
            }
            else
            {
                foreach (var p in TcpCollection.Instance)
                {
                    p.ReceiveDataAction = null;
                    p.ConnectStatsAction = null;
                    p.ShowMessageAction = null;
                }
            }
        }
        void DispPort()
        {
            BindPort(false);        
            TcpCollection.ClosePort();
        }
        private void FrmMain_Load(object sender, EventArgs e)
        {
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            mXlm.XmlToTreeView(treeView_product, xmlPath);
            this.treeView_product.ExpandAll();
            mOperation.PushErrMsg += new Fileoperation.PushEventHandler(ReceiveFileMsg);      
        }
        /// <summary>
        /// 接收来自文件操作类的消息
        /// </summary>
        /// <param name="msg"></param>
        private void ReceiveFileMsg(string msg)
        {
            if (msg != null)
                Monitor(msg, 3);
        }
        List<double> MarkXs = new List<double>();
        List<double> MarkYs = new List<double>();
        
        /// <summary>
        /// 计算机器人旋转中心
        /// </summary>
        /// <param name="HW"></param>
        /// <param name="msg"></param>
        private void GetRobotRotation(CogRecordDisplay HW, string msg)
        {
            string StrPath = Application.StartupPath + @"\Connetconfig.ini";
            double markX;
            double markY;
            int IndexCCD;
            double RobotRotationX;
            double RobotRotationY;
            string[] data;
            try
            {
                data = msg.Split(',');
                IndexCCD = Convert.ToInt32(data[1]);
                ICogRecord tmpRecord;
                switch (IndexCCD)
                {
                    case 0:
                        //mAcqCCD1 = mAcqFifoTools[0];
                        mAcqCCD1.Run();
                        HW.Image = mAcqCCD1.OutputImage as CogImage8Grey;
                        HW.Fit();
                        mGetRotationTool.Inputs["Image"].Value = mAcqCCD1.OutputImage as CogImage8Grey;
                        mCalibrationTool.Inputs["RunMode"].Value = 0;
                        mGetRotationTool.Run();
                        markX = (double)mGetRotationTool.Outputs["PicX"].Value;
                        markY = (double)mGetRotationTool.Outputs["PicY"].Value;
                        MarkXs.Add(markX);
                        MarkYs.Add(markY);
                        tmpRecord = mGetRotationTool.CreateLastRunRecord().SubRecords[0];
                        HW.Record = tmpRecord;
                        HW.Fit();
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
                if (RobotXs.Count == 3)   //计算旋转中心
                {
                    mGetRotationTool.Inputs["MarkX1"].Value = MarkXs[0];
                    mGetRotationTool.Inputs["MarkX2"].Value = MarkXs[1];
                    mGetRotationTool.Inputs["MarkX3"].Value = MarkXs[2];
                    mGetRotationTool.Inputs["MarkY1"].Value = MarkYs[0];
                    mGetRotationTool.Inputs["MarkY2"].Value = MarkYs[1];
                    mGetRotationTool.Inputs["MarkY3"].Value = MarkYs[2];
                    mGetRotationTool.Inputs["RunMode"].Value = 1;
                    mGetRotationTool.Run();
                    RobotRotationX = (double)mGetRotationTool.Outputs["RotationX"].Value;
                    RobotRotationY = (double)mGetRotationTool.Outputs["RotationY"].Value;
                    mOperation.WirteIni("参数设置", "旋转中心X", RobotRotationX.ToString(), StrPath);
                    mOperation.WirteIni("参数设置", "旋转中心Y", RobotRotationY.ToString(), StrPath);
                    ParamDatas.RobotRotationX = RobotRotationX.ToString();
                    ParamDatas.RobotRotationY = RobotRotationY.ToString();
                    tmpRecord = mGetRotationTool.CreateLastRunRecord().SubRecords[2];
                    HW.Record = tmpRecord;
                    HW.Fit();
                    MarkXs.Clear();
                    MarkYs.Clear();
                }
            }
            catch (Exception ex)
            {
                Monitor("获取旋转中心失败：" + ex.Message, 3);
                MarkXs.Clear();
                MarkYs.Clear();
            }
        }

        List<double> RobotXs = new List<double>();  //机器坐标X数组
        List<double> RobotYs = new List<double>();  //机器坐标y数组
        List<double> CCDXs = new List<double>();  //相机坐标X数组
        List<double> CCDYs = new List<double>();  //相机坐标y数组

        /// <summary>
        /// 自动标定
        /// </summary>
        /// <param name="msg"></param>
        private void AutoCalibration(CogRecordDisplay HW, string msg)
        {
            double RobotX;
            double RobotY;
            double CCDX;
            double CCDY;
            int IndexCCD;
            string[] data;
            try
            {
                data = msg.Split(',');
                IndexCCD = Convert.ToInt16(data[1]);
                RobotX = Convert.ToDouble(data[2]);
                RobotY = Convert.ToDouble(data[3]);
                RobotXs.Add(RobotX);
                RobotYs.Add(RobotY);
                ICogRecord tmpRecord;
                switch (IndexCCD)
                {
                    case 0:
                        mAcqCCD1.Run();
                        HW.Image = mAcqCCD1.OutputImage as CogImage8Grey;
                        HW.Fit();
                        mCalibrationTool.Inputs["Image"].Value = mAcqCCD1.OutputImage as CogImage8Grey;
                        mCalibrationTool.Inputs["RunMode"].Value = 0;
                        mCalibrationTool.Inputs["IsCalibration"].Value = false;
                        mCalibrationTool.Run();
                        CCDX = (double)mCalibrationTool.Outputs["PicX"].Value;
                        CCDY = (double)mCalibrationTool.Outputs["PicY"].Value;
                        CCDXs.Add(CCDX);
                        CCDYs.Add(CCDY);
                        tmpRecord = mCalibrationTool.CreateLastRunRecord().SubRecords[0];
                        HW.Record = tmpRecord;
                        HW.Fit();
                        if (RobotXs.Count == 9)   //计算校正
                        {
                            mCalibrationTool.Inputs["RobotX1"].Value = RobotXs[0];
                            mCalibrationTool.Inputs["RobotX2"].Value = RobotXs[1];
                            mCalibrationTool.Inputs["RobotX3"].Value = RobotXs[2];
                            mCalibrationTool.Inputs["RobotX4"].Value = RobotXs[3];
                            mCalibrationTool.Inputs["RobotX5"].Value = RobotXs[4];
                            mCalibrationTool.Inputs["RobotX6"].Value = RobotXs[5];
                            mCalibrationTool.Inputs["RobotX7"].Value = RobotXs[6];
                            mCalibrationTool.Inputs["RobotX8"].Value = RobotXs[7];
                            mCalibrationTool.Inputs["RobotX9"].Value = RobotXs[8];
                            mCalibrationTool.Inputs["RobotY1"].Value = RobotYs[0];
                            mCalibrationTool.Inputs["RobotY2"].Value = RobotYs[1];
                            mCalibrationTool.Inputs["RobotY3"].Value = RobotYs[2];
                            mCalibrationTool.Inputs["RobotY4"].Value = RobotYs[3];
                            mCalibrationTool.Inputs["RobotY5"].Value = RobotYs[4];
                            mCalibrationTool.Inputs["RobotY6"].Value = RobotYs[5];
                            mCalibrationTool.Inputs["RobotY7"].Value = RobotYs[6];
                            mCalibrationTool.Inputs["RobotY8"].Value = RobotYs[7];
                            mCalibrationTool.Inputs["RobotY9"].Value = RobotYs[8];
                         
                            mCalibrationTool.Inputs["CCDX1"].Value = CCDXs[0];
                            mCalibrationTool.Inputs["CCDX2"].Value = CCDXs[1];
                            mCalibrationTool.Inputs["CCDX3"].Value = CCDXs[2];
                            mCalibrationTool.Inputs["CCDX4"].Value = CCDXs[3];
                            mCalibrationTool.Inputs["CCDX5"].Value = CCDXs[4];
                            mCalibrationTool.Inputs["CCDX6"].Value = CCDXs[5];
                            mCalibrationTool.Inputs["CCDX7"].Value = CCDXs[6];
                            mCalibrationTool.Inputs["CCDX8"].Value = CCDXs[7];
                            mCalibrationTool.Inputs["CCDX9"].Value = CCDXs[8];
                            mCalibrationTool.Inputs["CCDY1"].Value = CCDYs[0];
                            mCalibrationTool.Inputs["CCDY2"].Value = CCDYs[1];
                            mCalibrationTool.Inputs["CCDY3"].Value = CCDYs[2];
                            mCalibrationTool.Inputs["CCDY4"].Value = CCDYs[3];
                            mCalibrationTool.Inputs["CCDY5"].Value = CCDYs[4];
                            mCalibrationTool.Inputs["CCDY6"].Value = CCDYs[5];
                            mCalibrationTool.Inputs["CCDY7"].Value = CCDYs[6];
                            mCalibrationTool.Inputs["CCDY8"].Value = CCDYs[7];
                            mCalibrationTool.Inputs["CCDY9"].Value = CCDYs[8];
                            mCalibrationTool.Inputs["RunMode"].Value = 1;
                            mCalibrationTool.Inputs["IsCalibration"].Value = true;
                            mCalibrationTool.Run();
                            tmpRecord = mCalibrationTool.CreateLastRunRecord().SubRecords[2];
                            HW.Record = tmpRecord;
                            HW.Fit();
                            RobotXs.Clear();
                            RobotYs.Clear();
                            CCDXs.Clear();
                            CCDYs.Clear();
                        }
                        break;
                    case 1:
                        mAcqCCD2.Run();
                        HW.Image = mAcqCCD2.OutputImage as CogImage8Grey;
                        HW.Fit();
                        mCalibrationTool1.Inputs["Image"].Value = mAcqCCD2.OutputImage as CogImage8Grey;
                        mCalibrationTool1.Inputs["RunMode"].Value = 0;
                        mCalibrationTool1.Inputs["IsCalibration"].Value = false;
                        mCalibrationTool1.Run();
                        CCDX = (double)mCalibrationTool1.Outputs["PicX"].Value;
                        CCDY = (double)mCalibrationTool1.Outputs["PicY"].Value;
                        CCDXs.Add(CCDX);
                        CCDYs.Add(CCDY);
                        tmpRecord = mCalibrationTool1.CreateLastRunRecord().SubRecords[0];
                        HW.Record = tmpRecord;
                        HW.Fit();
                        if (RobotXs.Count == 4)   //计算校正
                        {
                            mCalibrationTool1.Inputs["RobotX1"].Value = RobotXs[0];
                            mCalibrationTool1.Inputs["RobotX2"].Value = RobotXs[1];
                            mCalibrationTool1.Inputs["RobotX3"].Value = RobotXs[2];
                            mCalibrationTool1.Inputs["RobotX4"].Value = RobotXs[3];
                            mCalibrationTool1.Inputs["RobotX5"].Value = RobotXs[4];
                            mCalibrationTool1.Inputs["RobotX6"].Value = RobotXs[5];
                            mCalibrationTool1.Inputs["RobotX7"].Value = RobotXs[6];
                            mCalibrationTool1.Inputs["RobotX8"].Value = RobotXs[7];
                            mCalibrationTool1.Inputs["RobotX9"].Value = RobotXs[8];
                            mCalibrationTool1.Inputs["RobotY1"].Value = RobotYs[0];
                            mCalibrationTool1.Inputs["RobotY2"].Value = RobotYs[1];
                            mCalibrationTool1.Inputs["RobotY3"].Value = RobotYs[2];
                            mCalibrationTool1.Inputs["RobotY4"].Value = RobotYs[3];
                            mCalibrationTool1.Inputs["RobotY5"].Value = RobotYs[4];
                            mCalibrationTool1.Inputs["RobotY6"].Value = RobotYs[5];
                            mCalibrationTool1.Inputs["RobotY7"].Value = RobotYs[6];
                            mCalibrationTool1.Inputs["RobotY8"].Value = RobotYs[7];
                            mCalibrationTool1.Inputs["RobotY9"].Value = RobotYs[8];

                            mCalibrationTool1.Inputs["CCDX1"].Value = CCDXs[0];
                            mCalibrationTool1.Inputs["CCDX2"].Value = CCDXs[1];
                            mCalibrationTool1.Inputs["CCDX3"].Value = CCDXs[2];
                            mCalibrationTool1.Inputs["CCDX4"].Value = CCDXs[3];
                            mCalibrationTool1.Inputs["CCDX5"].Value = CCDXs[4];
                            mCalibrationTool1.Inputs["CCDX6"].Value = CCDXs[5];
                            mCalibrationTool1.Inputs["CCDX7"].Value = CCDXs[6];
                            mCalibrationTool1.Inputs["CCDX8"].Value = CCDXs[7];
                            mCalibrationTool1.Inputs["CCDX9"].Value = CCDXs[8];
                            mCalibrationTool1.Inputs["CCDY1"].Value = CCDYs[0];
                            mCalibrationTool1.Inputs["CCDY2"].Value = CCDYs[1];
                            mCalibrationTool1.Inputs["CCDY3"].Value = CCDYs[2];
                            mCalibrationTool1.Inputs["CCDY4"].Value = CCDYs[3];
                            mCalibrationTool1.Inputs["CCDY5"].Value = CCDYs[4];
                            mCalibrationTool1.Inputs["CCDY6"].Value = CCDYs[5];
                            mCalibrationTool1.Inputs["CCDY7"].Value = CCDYs[6];
                            mCalibrationTool1.Inputs["CCDY8"].Value = CCDYs[7];
                            mCalibrationTool1.Inputs["CCDY9"].Value = CCDYs[8];
                            mCalibrationTool1.Inputs["RunMode"].Value = 1;
                            mCalibrationTool1.Inputs["IsCalibration"].Value = true;
                            mCalibrationTool1.Run();
                            tmpRecord = mCalibrationTool1.CreateLastRunRecord().SubRecords[2];
                            HW.Record = tmpRecord;
                            HW.Fit();
                            RobotXs.Clear();
                            RobotYs.Clear();
                            CCDXs.Clear();
                            CCDYs.Clear();
                        }
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Monitor("标定失败：" + ex.Message, 3);
                RobotXs.Clear();
                RobotYs.Clear();
                CCDXs.Clear();
                CCDYs.Clear();
            }
        }
        CogRecordDisplay hwindow = new CogRecordDisplay();
        CogRecordDisplay hwindow1 = new CogRecordDisplay();
        CogRecordDisplay hwindow2 = new CogRecordDisplay();
        CogRecordDisplay hwindow3 = new CogRecordDisplay();
        TextBox TextMessage = new TextBox();
        List<CogRecordDisplay> hwindows = new List<CogRecordDisplay>();

       

        private void ShowMark()
        {
            for (int i = 1; i <= 4; i++)
            {
                switch (i)
                {
                    case 1:
                        Creatgraphs(hwindow, i);
                        break;
                    case 2:
                        Creatgraphs(hwindow1, i);
                        break;

                    default:
                        break;
                }
            }
        }
        /// <summary>
        /// 创建坐标系
        /// </summary>
        /// <param name="HW">=显示的窗口</param>
        /// <param name="index">窗口序列</param>
        private void Creatgraphs(CogRecordDisplay HW, int index)
        {
            CogImageConvert cc = new CogImageConvert();
           
            //画X轴
            CogLineSegment cgLineSement = new CogLineSegment();
            cgLineSement.StartX = HW.Width / 2;
            cgLineSement.StartY = HW.Height / 2;
            cgLineSement.EndX = cgLineSement.StartX + HW.Width / 4;
            cgLineSement.EndY = cgLineSement.StartY;
            cgLineSement.Color = CogColorConstants.Yellow;
            cgLineSement.SelectedSpaceName = "*";
            cgLineSement.Interactive = true;
            HW.InteractiveGraphics.Add(cgLineSement, null, false);
            //画Y轴
            CogLineSegment cgLineSement1 = new CogLineSegment();
            cgLineSement1.StartX = HW.Width / 2;
            cgLineSement1.StartY = HW.Height / 2;
            cgLineSement1.EndX = cgLineSement1.StartX;
            cgLineSement1.EndY = cgLineSement1.StartY + HW.Width / 4;
            cgLineSement1.Color = CogColorConstants.Yellow;
            cgLineSement1.SelectedSpaceName = "*";
            cgLineSement1.Interactive = true;
            HW.InteractiveGraphics.Add(cgLineSement1, null, false);
            //画X轴箭头
            CogLineSegment cgLineSement2 = new CogLineSegment();
            cgLineSement2.StartX = cgLineSement.EndX;
            cgLineSement2.StartY = cgLineSement.EndY;
            cgLineSement2.EndX = cgLineSement.EndX - 10;
            cgLineSement2.EndY = cgLineSement.EndY - 10;
            cgLineSement2.Color = CogColorConstants.Yellow;
            cgLineSement2.SelectedSpaceName = "*";
            cgLineSement2.Interactive = true;
            HW.InteractiveGraphics.Add(cgLineSement2, null, false);
            CogLineSegment cgLineSement3 = new CogLineSegment();
            cgLineSement3.StartX = cgLineSement.EndX;
            cgLineSement3.StartY = cgLineSement.EndY;
            cgLineSement3.EndX = cgLineSement.EndX - 10;
            cgLineSement3.EndY = cgLineSement.EndY + 10;
            cgLineSement3.Color = CogColorConstants.Yellow;
            cgLineSement3.SelectedSpaceName = "*";
            cgLineSement3.Interactive = true;
            HW.InteractiveGraphics.Add(cgLineSement3, null, false);
            //画Y轴箭头
            CogLineSegment cgLineSement4 = new CogLineSegment();
            cgLineSement4.StartX = cgLineSement1.EndX;
            cgLineSement4.StartY = cgLineSement1.EndY;
            cgLineSement4.EndX = cgLineSement1.EndX - 10;
            cgLineSement4.EndY = cgLineSement1.EndY - 10;
            cgLineSement4.Color = CogColorConstants.Yellow;
            cgLineSement4.SelectedSpaceName = "*";
            cgLineSement4.Interactive = true;
            HW.InteractiveGraphics.Add(cgLineSement4, null, false);
            CogLineSegment cgLineSement5 = new CogLineSegment();
            cgLineSement5.StartX = cgLineSement1.EndX;
            cgLineSement5.StartY = cgLineSement1.EndY;
            cgLineSement5.EndX = cgLineSement1.EndX + 10;
            cgLineSement5.EndY = cgLineSement1.EndY - 10;
            cgLineSement5.Color = CogColorConstants.Yellow;
            cgLineSement5.SelectedSpaceName = "*";
            cgLineSement5.Interactive = true;
            HW.InteractiveGraphics.Add(cgLineSement5, null, false);
            //画XY轴显示信息
            CogGraphicLabel LableX = new CogGraphicLabel();
            LableX.Color = CogColorConstants.Blue;
            LableX.BackgroundColor = CogColorConstants.Cyan;
            LableX.X = cgLineSement.EndX + 10;
            LableX.Y = cgLineSement.EndY + 15;
            LableX.Interactive = true;
            LableX.SelectedSpaceName = "*";
            CogGraphicLabel LableY = new CogGraphicLabel();
            LableY.Color = CogColorConstants.Blue;
            LableY.BackgroundColor = CogColorConstants.Cyan;
            LableY.X = cgLineSement1.EndX + 20;
            LableY.Y = cgLineSement1.EndY + 5;
            LableY.Interactive = true;
            LableY.SelectedSpaceName = "*";
            switch (index)
            {
                case 1:
                    LableX.Text = "X-";
                    LableY.Text = "Y-";
                    break;
                case 2:
                    LableX.Text = "Y-";
                    LableY.Text = "X-";
                    break;
                case 3:
                    LableX.Text = "Y+";
                    LableY.Text = "X+";
                    break;
                case 4:
                    LableX.Text = "Y+";
                    LableY.Text = "X+";
                    break;
                default:
                    break;

            }
            HW.InteractiveGraphics.Add(LableX, null, false);
            HW.InteractiveGraphics.Add(LableY, null, false);
        }
        /// <summary>
        /// 自动适应屏幕分辨率
        /// </summary>
        private void AutoSizeScreen()
        {           
            int productToolWidth = this.groupBox1.Width;
            int menuHight = 25; //菜单栏高度
            int ScreenWidth = Screen.PrimaryScreen.Bounds.Width;    //获取当前屏幕宽度
            int ScreenHight = Screen.PrimaryScreen.WorkingArea.Height;   //获取当前屏幕高度
            int messageTextWidth = 0;//信息显示栏宽度
            int messageTextHeight=0;//信息显示栏高度
            double scale = ScreenWidth / 1366;
            double hwX; //
            double hwY;
            int Lx=0;
            int x=0;
            int y=0;
           int hwNumber = mCheckPaths.Count;//获取当前检测vpp数量
            if( hwNumber ==0)
            {
                Monitor("当前路径下无vpp！", 3);
                return;             
            }
                  
            foreach (CogRecordDisplay mhw in hwindows)
            {
                this.Controls.Remove(mhw);
            }
            hwindows.Clear();
            if(hwNumber <4 ) //主界面显示一排
            {
                messageTextWidth = ScreenWidth;
                messageTextHeight=250;
                if (this.groupBox1.Visible)
                {                                       
                    hwX = (ScreenWidth - productToolWidth) / hwNumber;
                    Lx = productToolWidth;
                }
                else
                {
                    hwX = ScreenWidth  / hwNumber;
                    Lx = 0;
                }
                hwY = (ScreenHight - menuHight-messageTextHeight);
                x = (int)hwX;
                 y = (int)hwY;

                 for (int i = 0; i < hwNumber; i++)
                 {
                     CogRecordDisplay hw = new CogRecordDisplay();                               
                     hw.Width = x;
                     hw.Height = y;
                    hw.Location = new Point(Lx + i * x, menuHight);
                    this.Controls.Add(hw);
                     hwindows.Add(hw);
                 }
                 TextMessage.ScrollBars = ScrollBars.Vertical;
                  TextMessage.Multiline = true;        
                 TextMessage.Width = messageTextWidth;
                TextMessage.Height = messageTextHeight;
                TextMessage.Location = new Point(Lx, y + 30);
                this.Controls.Add(TextMessage);
            }
                else   //主界面分两排显示
            {
                int num = hwNumber / 2;
                messageTextWidth = 280;
                messageTextHeight = ScreenHight;
                if (this.groupBox1.Visible)
                {
                    hwX = (ScreenWidth - productToolWidth - messageTextWidth) / num;
                    Lx = productToolWidth;
                }
                else
                {
                    hwX = (ScreenWidth -messageTextWidth)/ num;
                    Lx = 0;
                }
                hwY = (ScreenHight - menuHight)/2;
                x = (int)hwX;
                y = (int)hwY;

                for (int i = 0; i < hwNumber; i++)
                {
                    CogRecordDisplay hw = new CogRecordDisplay();
                    hw.Width = x;
                    hw.Height = y;
                    if (i <num)  //第一排hwindow
                    {
                        hw.Location = new Point(Lx + i * x, menuHight);
                    }
                    else      //第二排hwindow
                    {
                        hw.Location = new Point(Lx + (i-num)  * x, menuHight+y);
                    }                
                    this.Controls.Add(hw);
                    hwindows.Add(hw);
                }
                TextMessage.ScrollBars = ScrollBars.Vertical;
                TextMessage.Multiline = true;
                TextMessage.Width = messageTextWidth;
                TextMessage.Height = messageTextHeight;
                TextMessage.Location = new Point(Lx + num * x, menuHight);
                this.Controls.Add(TextMessage);
            }       
        }
        #region 通讯处理（TCP/IP、RS232、IO)

        int nCard = 0;
        /// <summary>
        /// 初始化IO卡
        /// </summary>
        /// <param name="value"></param>
        private void InitializeIOCard(object value)
        {
            nCard = IOC0640.ioc_board_init();
            if (nCard > 0)//控制卡初始化
            {
                Monitor("IO控制卡初始化成功!", 3);
                //ReadIOThread = new Thread(ReadIO);  //开启读IO卡线程
                ReadIOThread.Start();
            }
            else
            {
                Monitor("IO控制卡初始化失败：未找到IO控制卡!", 3);
            }

            MessageBox.Show("未找到IO控制卡!", "警告");

        }

        /// <summary>
        /// 初始化通讯
        /// </summary>
        //private void InitializeConnect(object value)
        //{
        //    ConnectModel = ParamDatas.ConnectModel; //确认是通讯是tcp还是Rs232
        //    switch (ConnectModel)
        //    {
        //        case "网口通信":
        //            {
        //                //InitializeTcp();
        //                break;
        //            }
        //        case "串口通信":
        //            {
        //                InitializeRS232();
        //                break;
        //            }
        //        default:
        //            Monitor("未配置正确的通信模:" + ConnectModel, 3);
        //            break;
        //    }
        //}
        /// <summary>
        /// 初始化网口通信
        /// </summary>
        //private void InitializeTcp()
        //{
        //    string TcpModel = ParamDatas.TcpMolde; //确认是服务器还是客户端

        //    switch (TcpModel)
        //    {
        //        case "服务器":
        //            {
        //                InitializeTcpServer();
        //                break;
        //            }
        //        case "客户端":
        //            {
        //                InitializeTcpClient("1");
        //                //ReconnectThread = new Thread(Reconnect);
        //                //ReconnectThread.Start();
        //                break;
        //            }
        //        default:
        //            Monitor("未配置正确的网口" + TcpModel, 3);
        //            break;
        //    }
        //}

        SerialPortUtil mSport;//Serial端口
                              /// <summary>
                              /// 初始化串口通信
                              /// </summary>
        //private void InitializeRS232()
        //{
        //    try
        //    {
        //        mSport = new SerialPortUtil(ParamDatas.serialPort, ParamDatas.Baudrate, ParamDatas.Parity, ParamDatas.DataBits, ParamDatas.StopBits);
        //        if (!mSport.IsOpen)
        //        {
        //            mSport.OpenPort();
        //            Monitor(mSport.PortName.ToString() + "启动成功", 3);
        //            mSport.WriteData("SA0999TC#");  //打开CG环形光，接控制器CH1
        //            mSport.WriteData("SB0999TC#");  //打卡CG同轴光，接控制器CH2
        //            Thread.Sleep(150);
        //            mSport.WriteData("SA0000FC#");  //关闭CG环形光，接控制器CH1
        //            mSport.WriteData("SB0000FC#");  //关闭CG同轴光，接控制器CH2
        //        }
        //        mSport.PushSerial += new SerialPortUtil.PushEventHandler(ReceiveSerialPortMsg);
        //    }
        //    catch (Exception ex)
        //    {
        //        Monitor("COM端口启动失败:" + ex.Message, 3);
        //    }
        //}
        /// <summary>
        /// 启动tcp服务器
        /// </summary>
        //private void InitializeTcpServer()
        //{
        //    try
        //    {
        //        mServerIp = IPAddress.Parse(ParamDatas.IpAddress);
        //        mPort = Convert.ToInt16(ParamDatas.TcpPort);
        //        SocketHelper.pushSockets = new SocketHelper.PushSockets(ReceiveClientMsg);
        //        mTcpServer = new SocketHelper.TcpServer();
        //        mTcpServer.InitSocket(mServerIp, mPort);
        //        mTcpServer.Start();
        //        Monitor("服务器启动成功，等待客户端连接……", 3);
        //    }
        //    catch (Exception ex)
        //    {
        //        Monitor("服务器启动失败:" + ex.Message, 3);
        //    }
        //}
        //bool mCleintStation;
        ///// <summary>
        ///// 连接Tcp客户端
        ///// </summary>
        //private void InitializeTcpClient(object value)
        //{
        //    try
        //    {
        //        watch = Stopwatch.StartNew();
        //        mClientIp = IPAddress.Parse(ParamDatas.IpAddress);
        //        mPort = Convert.ToInt16(ParamDatas.TcpPort);
        //        mTcpClient = new SocketHelper.TcpClients();
        //        mTcpClient.InitSocket(mClientIp, mPort);
        //        mTcpClient.Start();
        //        SocketHelper.pushSockets = new SocketHelper.PushSockets(ReceiveServerMsg);//注册推送器
        //        mCleintStation = true;
        //        Monitor("连接服务器成功!", 3);
        //    }
        //    catch (Exception ex)
        //    {
        //        Monitor("连接服务器失败:" + ex.Message, 3);
        //        //Thread.Sleep(3000); //每隔3秒重新连接一次
        //        //ReconnectNums+=1;
        //        //if (ReconnectNums>100)
        //        //{
        //        //    Monitor("重连超出最大次数，请检查服务器是否开启,并且重启软件!", 3);
        //        //    ReconnectNums = 0;
        //        //    ReconnectStation = true;   //重连状态结束
        //        //    return ;
        //        //}
        //        //    Monitor("尝试第"+ReconnectNums.ToString()+"次连接服务器……", 3);
        //        //    InitializeTcpClient();                            
        //    }
        //}

        /// <summary>
        /// 客户端掉线后重连服务器
        /// </summary>
        //private void Reconnect(string name)
        //{
        //    switch (name)
        //    {
        //        case "Server":
        //            if (!mCleintStation)
        //            {
        //                ThreadPool.QueueUserWorkItem(new WaitCallback(InitializeTcpClient));
        //            }
        //            break;


        //        default:
        //            break;

        //    }
        //while (true )
        //{

        //   // if (!ReconnectStation & !mCleintStation)
        //   // {
        //   //     Thread.Sleep(2000);//延时2s取捕获客户端与服务器是否失去连接

        //   //         //InitializeTcpClient();                
        //   //}

        //}         
        //}
        /// <summary>
        /// 接收串口数据
        /// </summary>
        /// <param name="Received">=接收到的数据</param>
        private void ReceiveSerialPortMsg(string Received)
        {
            try
            {
                string msg = string.Empty;
                Thread.Sleep(10);
                msg = Received;
                // Monitor(msg, 3);
                //  ReceiveMsgProcess(msg);
            }
            catch (Exception ex)
            {
                Monitor("串口数据接收异常:" + ex.Message, 3);
            }
        }
        ///// <summary>
        ///// 接收来自服务器消息
        ///// </summary>
        ///// <param name="sks"></param>
        //private void ReceiveServerMsg(SocketHelper.Sockets sks)
        //{
        //    int sport = 0;

        //    if (sks.ex != null)
        //    {
        //        if (sks.ClientDispose == true)
        //        {
        //            //由于未知原因引发异常.导致客户端下线.   比如网络故障.或服务器断开连接.
        //            mTcpClient.Stop();
        //            Monitor((string.Format("与服务器失去连接!")), 3);
        //            mCleintStation = false;
        //            return;
        //        }
        //    }
        //    else if (sks.Offset == 0)
        //    {
        //        //客户端主动下线               
        //        mTcpClient.Stop();
        //        Monitor((string.Format("与服务器失去连接!")), 3);
        //        mCleintStation = false;
        //        return;
        //    }
        //    else
        //    {
        //        sport = sks.Ip.Port;  //获取当前服务器端口，判断是来自读码器还是运动控制
        //        byte[] buffer = new byte[sks.Offset];
        //        Array.Copy(sks.RecBuffer, buffer, sks.Offset);
        //        ReceiveData = Encoding.UTF8.GetString(buffer);
        //        Monitor(ReceiveData, 1);
        //        if (ReceiveData != null)
        //            ReceiveMsgProcess(ReceiveData);
        //    }
        //}
        ///// <summary>
        ///// 接收来自客户端消息
        ///// </summary>
        ///// <param name="sks"></param>
        //private void ReceiveClientMsg(SocketHelper.Sockets sks)
        //{
        //    if (sks.ex != null)
        //    {
        //        //在此处理异常信息
        //        if (sks.ClientDispose)
        //        {
        //            //客户端非主动断开连接下线.  非正常下线  
        //            string str = string.Format("客户端:{0}下线.!", sks.Ip);
        //            Monitor(str, 3);

        //        }
        //        MessageBox.Show(sks.ex.Message);
        //        //服务端  server.ClientList 已移除,需要用户再次移除
        //    }
        //    else
        //    {
        //        if (sks.NewClientFlag)
        //        {
        //            string str = string.Format("新客户端:{0}连接成功.!", sks.Ip);

        //            Monitor(str, 3);

        //        }
        //        else if (sks.Offset == 0)
        //        {
        //            //正常是不会发送0包的,只有客户端主动断开连接时发送空包.

        //            string str = string.Format("客户端:{0}下线.!", sks.Ip);
        //            Monitor(str, 3);
        //        }
        //        else
        //        {
        //            mUsedIP = sks.Ip;    //最新绑定的客户端
        //            byte[] buffer = new byte[sks.Offset];
        //            Array.Copy(sks.RecBuffer, buffer, sks.Offset);
        //            ReceiveData = Encoding.UTF8.GetString(buffer);
        //            Monitor(ReceiveData, 1);
        //            ReceiveMsgProcess(ReceiveData);
        //        }
        //    }
        //}

        /// <summary>
        /// 处理接收的数据
        /// </summary>
        /// <param name="msg">处理流程标识信息</param>
        //private void ReceiveMsgProcess(string msg)
        //{
        //    string workmsg = "";
        //    //string product = "";
        //    if (msg != null)
        //    {
        //        string[] data = msg.Split(';');
        //        //string[] data = msg.Split(',');
        //        switch (data.Length)
        //        {
        //            case 2:
        //                workmsg = data[1];
        //                //   product = data[1];
        //                break;
        //            case 3:
        //                workmsg = data[1] + data[2];
        //                break;
        //            default:
        //                break;
        //        }
        //        switch (workmsg)
        //        {
        //            case "grabccd1":    // CCD1工作
        //                CCD1Work(mProductCode, hwindow);
        //                break;
        //            case "grabccd2":    // CCD2工作
        //                mProductCode = "";
        //                CCD2Work(mProductCode, hwindow);
        //                break;
        //            case "grabccd3":    //CCD3工作
        //                mProductCode = "";
        //                CCD3Work(mProductCode, hwindow1);
        //                break;
        //            case "grabcalibration1":    //CCD1标定工作
        //                AutoCalibration(hwindow, msg);
        //                break;
        //            case "grabcalibration2": // CCD2标定工作
        //                AutoCalibration(hwindow1, msg);
        //                break;
        //            case "grabcalibration3":   //CCD3标定工作
        //                AutoCalibration(hwindow2, msg);
        //                break;
        //            case "getrbot":      //获取机器人旋转中心
        //                GetRobotRotation(hwindow, msg);
        //                break;
        //            case "rename":  //产品重命名
        //                break;
        //            case "delete":  //删除产品
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //}

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="msg">发送的类容</param>
        /// <param name="SendType">发送协议，0为Server,1为Client,2为Serail</param>
        /// 
        //private void SendMsg(string msg, int SendType)
        //{
        //    try
        //    {
        //        switch (SendType)
        //        {
        //            case 0:
        //                //mTcpServer.SendToClient(mUsedIP, msg);  //Server向指定的Client端口发送message
        //                break;
        //            case 1:
        //                //mTcpClient.SendData(msg);              //Client向Server发送message
        //                break;
        //            case 2:
        //                mSport.WriteData(msg);                //RS232发送message
        //                break;
        //            default:
        //                break;
        //        }
        //        Monitor(msg, 2);
        //    }
        //    catch (Exception ex)
        //    {
        //        Monitor("数据发送失败" + ex.Message, 3);
        //    }
        //}

        #endregion

        #region 文件处理  
        /// <summary>
        /// 数据处理
        /// </summary>
        /// <param name="value"></param>
        private void DataProcess(object value)
        {

        }
        // DataGridView dgvSearchResult;
        /// <summary>
        /// 将DataGridView中数据导入CSV文件
        /// </summary>
        /// <param name="dgvSearchResult"></param>
        private void SaveDataGridViewData(DataGridView dgvSearchResult, string CCDID)
        {
            try
            {
                string datename = DateTime.Now.ToString("yyyy-MM-dd");
                string path = Application.StartupPath + "\\Data\\" + datename;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string delimiter = ",";
                string outputFilename = DateTime.Now.ToString("HHmmss") + "_" + CCDID + ".csv";
                string fullFilename = path + "\\" + outputFilename;
                StreamWriter csvStreamWriter = new StreamWriter(fullFilename, false, System.Text.Encoding.UTF8);

                //output header data
                string strHeader = "";
                for (int i = 0; i < dgvSearchResult.Columns.Count; i++)
                {
                    strHeader += dgvSearchResult.Columns[i].HeaderText + delimiter;
                }
                csvStreamWriter.WriteLine(strHeader);

                //output rows data
                for (int j = 0; j < dgvSearchResult.Rows.Count; j++)
                {
                    string strRowValue = "";

                    for (int k = 0; k < dgvSearchResult.Columns.Count; k++)
                    {
                        strRowValue += dgvSearchResult.Rows[j].Cells[k].Value + delimiter;

                    }
                    csvStreamWriter.WriteLine(strRowValue);
                }
                csvStreamWriter.Close();
            }
            catch (Exception ex)
            {
                Monitor("保存csv文件异常：" + ex.Message, 3);
            }
        }

        /// <summary>
        /// 保存原始图片
        /// </summary>
        /// <param name="value"></param>
        private void SaveRawImage(object value)
        {
            string s = (string)value;
            string[] data = s.Split(',');
            string path = data[2];
            string hw = data[1];
            string imagename = data[0];
            ICogImage image;

            switch (hw)
            {
                case "CCD1":
                    image = hwindows[0].Image;
                    mOperation.SaveImage(image, imagename, path);
                    break;
                case "CCD2":
                    image = hwindows[1].Image;
                    mOperation.SaveImage(image, imagename, path);
                    break;
                case "CCD3":
                    image = hwindows[2].Image;
                    mOperation.SaveImage(image, imagename, path);
                    break;

                default:
                    break;
            }
        }
        /// <summary>
        /// 保存带处理工具图片
        /// </summary>
        /// <param name="value"></param>
        private void SaveProImage(object value)
        {
            string s = (string)value;
            string[] data = s.Split(',');
            string imagename = data[0];
            string hw = data[1];
            string path = data[2];
            switch (hw)
            {
                case "CCD1":
                    mOperation.SaveProcessImage(hwindows [0], imagename, path);
                    break;
                case "CCD2":
                    mOperation.SaveProcessImage(hwindows[1], imagename, path);
                    break;
                case "CCD3":
                    mOperation.SaveProcessImage(hwindows[2], imagename, path);
                    break;
                default:
                    break;
            }
        }
        #endregion

        # region 选择方案
        /// <summary>
        /// 切换方案
        /// </summary>
        /// <param name="selectPath">=要切换的方案</param>
        private void SelectSolution(string selectProduct)
        {
            string StrPath = Application.StartupPath + @"\Connetconfig.ini";
            try
            {
                string selectPath = Application.StartupPath + @"\Solutions\" + selectProduct;
                if (selectPath == ParamDatas.SolutionPath)
                {
                    Monitor("要切换的方案与当前方案一致，请重新选择……", 3);
                    return;
                }
                Monitor("方案切换中，请勿操作……：", 3);
                //先释放当前vpp

                //重新加载所选的vpp
                LoadVpp(selectPath);
                //更新配置
                ParamDatas.SolutionPath = selectPath;
                mOperation.WirteIni("方案设置", "当前方案", selectPath, StrPath);
            }
            catch (Exception ex)
            {
                Monitor("方案切换失败：" + ex.Message, 3);
            }
        }
        /// <summary>
        /// 保存当前方案
        /// </summary>
        private void SaveSolution()
        {
            string StrPath = Application.StartupPath + @"\Connetconfig.ini";
         
            mOperation.WirteIni("方案设置", "当前产品", mProductName , StrPath);
        }
        #endregion
        #region 工作流程   
        private object LockCCD1Work = new object();
        /// <summary>
        /// 相机1处理流程
        /// </summary>
        /// <param name="value">运行模式："test"下为手动加载，其余为相机取图</param>
        /// <param name="hwindow">FD图片显示窗口</param>
        /// 
        double CCD1CaliX;
        double CCD1CaliY;
        double CCD1CaliAngle;
        double CCD1MoveX;
        double CCD1MoveY;
        double CCD1MoveAngle;
        private void CCD1Work(object value, CogRecordDisplay mhwindow,string msg, TcpBase sender)
        {
            lock (LockCCD1Work)
            {
                string Path = productPath + "\\config.ini";
                string SendStr="";
                string Errmsg;
                string SN = (string)value;//触发模式，test为外部加载图像，其余为相机采图         
                string ProcessImagepath = SN + ",CCD1,D:\\" + "Image"+"\\ProImage"+"\\CCD1";  //带处理工具图片路径
                string Rawpath = SN + ",CCD1,D:\\" + "Image" + "\\RawImage" + "\\CCD1";  //原始图片路径
                double X=0;  //当前X
                double Y=0;  //当前Y
                //double Angle=0; //当前角度
                double RobotRotationX; //机器人旋转中心X
                double RobotRotationY; //机器人旋转中心Y
                double PMAlignAngle1=0;//模板角度值
                 //CCD1MoveX=0;
                //CCD1MoveY=0;
                // CCD1MoveAngle=0;
                 try
                 {
                     CogImage8Grey image;

                     RobotRotationX = Convert.ToDouble(ParamDatas.RobotRotationX);
                     RobotRotationY = Convert.ToDouble(ParamDatas.RobotRotationY);
                     if (SN != "test")  //外部图像
                     {
                         // mAcqCCD1.Run();
                         mAcqFifoTools[0].Run();
                         image = mAcqFifoTools[0].OutputImage as CogImage8Grey;
                         //cogToolBlockEditV21.Subject.Inputs["Image"].Value = mImage;
                     }
                     else
                     {
                         image = mImage;
                     }
                     if (image != null)
                     {
                         mhwindow.Image = image;
                         mhwindow.Fit();
                         mCheckCCDs[0].Inputs["Image"].Value = image;
                         mCheckCCDs[0].Inputs["CaliX"].Value = CCD1CaliX;
                         mCheckCCDs[0].Inputs["CaliY"].Value = CCD1CaliY;
                         mCheckCCDs[0].Inputs["CaliAngle"].Value = CCD1CaliAngle;
                         ICogRecord tmpRecord;
                         mCheckCCDs[0].Run();
                         
                         Errmsg = (string)mCheckCCDs[0].Outputs["Errmsg"].Value;
                         if (Errmsg == null)
                         {
                             X = (double)mCheckCCDs[0].Outputs["CenterX"].Value*(1000);
                             X = Convert.ToInt32(X);
                             Y = (double)mCheckCCDs[0].Outputs["CenterY"].Value * (1000);
                             Y = Convert.ToInt32(Y);
                             //Angle = (double)mCheckCCDs[0].Outputs["CenterAngle"].Value * (1000);
                             //Angle = Convert.ToInt32(Angle);
                             PMAlignAngle1 = (double)mCheckCCDs[0].Outputs["PMAlignAngle"].Value * (1000);
                             PMAlignAngle1 = Convert.ToInt32(PMAlignAngle1);
                             //PMAlignAngle1 = (double)mCheckCCDs[0].Outputs["PMAlignAngle"].Value * (1000);
                             //PMAlignAngle1 = Convert.ToInt32(PMAlignAngle1);
                             tmpRecord = mCheckCCDs[0].CreateLastRunRecord();
                             mhwindow.Record = tmpRecord;
                             mhwindow.Fit(true);
                             if (msg == "model1")//将当前值赋予基准值
                             {
                                 mOperation.WirteIni("参数设置", "CCD1基准X", X.ToString(), Path);
                                 mOperation.WirteIni("参数设置", "CCD1基准Y", Y.ToString(), Path);
                                 //mOperation.WirteIni("参数设置", "CCD1基准角度", Angle.ToString(), Path);
                                 UpadteParam(productPath);
                                 return;
                             }
                             //保存原始和处理后的图像
                             ThreadPool.QueueUserWorkItem(new WaitCallback(SaveProImage), ProcessImagepath);
                             ThreadPool.QueueUserWorkItem(new WaitCallback(SaveRawImage), Rawpath);

                             //SendStr = "return," + msg + "," + "1," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + PMAlignAngle1 +","+ productNum + ",\r\n";
                             SendStr = "return," + msg + "," + "1," + X.ToString() + "," + Y.ToString() + ","  + PMAlignAngle1 + "," + productNum + ",\r\n";
                         }
                         else//检测失败
                         {
                             //SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + PMAlignAngle1 + "," + productNum + ",\r\n";
                             SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + PMAlignAngle1.ToString() + "," + productNum + ",\r\n";
                         }
                     }
                     else //取图失败
                     {
                         //SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + PMAlignAngle1 + "," + productNum + ",\r\n";
                         SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + PMAlignAngle1.ToString() + "," + productNum + ",\r\n";
                     }
                 }

                 catch (Exception ex)
                 {

                     Monitor("相机1检测异常：" + ex.Message, 3);
                     //SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + PMAlignAngle1 + "," + productNum + ",\r\n";
                     SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + PMAlignAngle1.ToString() + "," + productNum + ",\r\n";

                 }
                finally 
                    {
                        if (sender != null & SN!="Test")
                           sender.Write(SendStr);
                    }
            }
        }
        private object LockCCD2Work = new object();
        double CCD2CaliX;
        double CCD2CaliY;
        double CCD2CaliAngle;
  
        /// <summary>
        /// 相机2处理流程
        /// </summary>
        /// <param name="value"></param>
        /// <param name="mhwindow"></param>
        private void CCD2Work(object value, CogRecordDisplay mhwindow,string msg, TcpBase sender)
        {
            lock (LockCCD2Work)
            {
               string  Path = productPath + "\\config.ini";
                string SendStr = "";
                string Errmsg;
                string SN = (string)value;//触发模式，test为外部加载图像，其余为相机采图         
                string ProcessImagepath = SN + ",CCD2,D:\\" + "Image" + "\\ProImage" + "\\CCD2";  //带处理工具图片路径
                string Rawpath = SN + ",CCD2,D:\\" + "Image" + "\\RawImage" + "\\CCD2";  //原始图片路径
                double X=0;  //当前X
                double Y=0;  //当前Y
                double Angle=0; //当前角度
              //  double RobotRotationX; //机器人旋转中心X
                //double RobotRotationY; //机器人旋转中心Y
            
                try
                {
                    CogImage8Grey image;                
                   
                    if (SN != "test")  //外部图像
                    {                       
                        mAcqFifoTools[1].Run();
                        image = mAcqFifoTools[1].OutputImage as CogImage8Grey;
                    }
                    else
                    {
                        image = mImage;
                    }
                    if (image != null)
                    {
                        mhwindow.Image = image;
                        mhwindow.Fit();

                        mCheckCCDs[1].Inputs["Image"].Value = image;
                        mCheckCCDs[1].Inputs["CaliX"].Value = CCD2CaliX;
                        mCheckCCDs[1].Inputs["CaliY"].Value = CCD2CaliY;
                        mCheckCCDs[1].Inputs["CaliAngle"].Value = CCD2CaliAngle;
                        ICogRecord tmpRecord;
                        mCheckCCDs[1].Run();
                        Errmsg = (string)mCheckCCDs[1].Outputs["Errmsg"].Value;
                        if (Errmsg == null)
                        {
                            X = (double)mCheckCCDs[1].Outputs["CenterX"].Value*(1000);
                            X = Convert.ToInt32(X);
                            Y = (double)mCheckCCDs[1].Outputs["CenterY"].Value*(1000);
                            Y = Convert.ToInt32(Y);
                            Angle = (double)mCheckCCDs[1].Outputs["CenterAngle"].Value*(1000);
                            Angle = Convert.ToInt32(Angle);
                            tmpRecord = mCheckCCDs[1].CreateLastRunRecord().SubRecords[0];
                            mhwindow.Record = tmpRecord;
                            mhwindow.Fit(true);
                            if (msg == "model2")//将当前值赋予基准值
                            {
                                mOperation.WirteIni("参数设置", "CCD2基准X", X.ToString(), Path);
                                mOperation.WirteIni("参数设置", "CCD2基准Y", Y.ToString(), Path);
                                mOperation.WirteIni("参数设置", "CCD2基准角度", Angle.ToString(), Path);
                                UpadteParam(productPath);
                                return;
                            }

                            //保存原始和处理后的图像
                            ThreadPool.QueueUserWorkItem(new WaitCallback(SaveProImage), ProcessImagepath);
                            ThreadPool.QueueUserWorkItem(new WaitCallback(SaveRawImage), Rawpath);

                            SendStr = "return," + msg + "," + "1," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";
                        }
                        else//检测失败
                        {
                            SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";
                        }
                    }
                    else//相机2取图失败
                    {
                        SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";
                    }
                }
                catch (Exception ex)
                {

                    Monitor("相机2检测异常：" + ex.Message, 3);
                    SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";

                }
                finally
                {
                    if (sender != null & SN != "Test")
                        sender.Write(SendStr);
                }
            }
        }

        private object LockCCD3Work = new object();
        double CCD3CaliX;
        double CCD3CaliY;
        double CCD3CaliAngle;      
        /// <summary>
        /// 相机3处理流程
        /// </summary>
        /// <param name="value">运行模式："test"下为手动模式，其余为自动模式</param>
        /// <param name="hwindow">BM图片显示窗口</param>
        private void CCD3Work(object value, CogRecordDisplay mhwindow, string msg,TcpBase sender)
        {

            lock (LockCCD3Work)
            {
                string Path = productPath + "\\config.ini";
                string SendStr = "";
                string Errmsg;
                string SN = (string)value;//触发模式，test为外部加载图像，其余为相机采图         
                string ProcessImagepath = SN + ",CCD3,D:\\" + "Image" + "\\ProImage" + "\\CCD3";  //带处理工具图片路径
                string Rawpath = SN + ",CCD3,D:\\" + "Image" + "\\RawImage" + "\\CCD3";  //原始图片路径
                double X = 0;  //当前X
                double Y = 0;  //当前Y
                double Angle = 0; //当前角度              
                try
                 {
                    CogImage8Grey image;
                    if (SN != "test")  //外部图像
                    {
                        mAcqFifoTools[2].Run();
                        image = mAcqFifoTools[2].OutputImage as CogImage8Grey;
                    }
                    else
                    {
                        image = mImage;
                    }
                    if (image != null)
                    {
                        mhwindow.Image = image;
                        mhwindow.Fit();
                        mCheckCCDs[2].Inputs["Image"].Value = image;
                        mCheckCCDs[2].Inputs["CaliX"].Value = CCD3CaliX;
                        mCheckCCDs[2].Inputs["CaliY"].Value = CCD3CaliY;
                        mCheckCCDs[2].Inputs["CaliAngle"].Value = CCD3CaliAngle;
                        ICogRecord tmpRecord;
                        mCheckCCDs[2].Run();
                        Errmsg = (string)mCheckCCDs[2].Outputs["Errmsg"].Value;
                        if (Errmsg == null)
                        {
                            X = (double)mCheckCCDs[2].Outputs["CenterX"].Value*(1000);
                            X = Convert.ToInt32(X);
                            Y = (double)mCheckCCDs[2].Outputs["CenterY"].Value*(1000);
                            Y = Convert.ToInt32(Y);
                            Angle = (double)mCheckCCDs[2].Outputs["CenterAngle"].Value*(1000);
                            Angle = Convert.ToInt32(Angle);
                            tmpRecord = mCheckCCDs[2].CreateLastRunRecord().SubRecords[0];
                            mhwindow.Record = tmpRecord;
                            mhwindow.Fit(true);
                            if (msg == "model3")//将当前值赋予基准值
                            {
                                mOperation.WirteIni("参数设置", "CCD3基准X", X.ToString(), Path);
                                mOperation.WirteIni("参数设置", "CCD3基准Y", Y.ToString(), Path);
                                mOperation.WirteIni("参数设置", "CCD3基准角度", Angle.ToString(), Path);
                                UpadteParam(productPath);
                                return;
                            }

                            //保存原始和处理后的图像
                            ThreadPool.QueueUserWorkItem(new WaitCallback(SaveProImage), ProcessImagepath);
                            ThreadPool.QueueUserWorkItem(new WaitCallback(SaveRawImage), Rawpath);
                            SendStr = "return," + msg + "," + "1," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";
                        }
                        else//检测失败
                        {
                            SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";
                            
                        }
                    }
                    else//相机3取像失败
                    {
                        SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";
                        
                    }
                }
                catch (Exception ex)
                {

                    Monitor("相机3检测异常：" + ex.Message, 3);
                    SendStr = "return," + msg + "," + "2," + X.ToString() + "," + Y.ToString() + "," + Angle.ToString() + "," + productNum + ",\r\n";

                }
                finally
                {
                    if (sender != null & SN != "Test")
                        sender.Write(SendStr);
                }
            }
        }    
        #endregion
        /// <summary>
        /// 加载Vpp程序
        /// </summary>
        private void LoadVpp(object value)
        {
            try
            {
                //toolStrip1.Enabled = false;
                string path = (string)value;
          
                Monitor("系统加载中……", 3);
                //加载取像vpp
                foreach (string  fileName in mAcqPaths)
                {
                    mAcqFifoTools.Add(CogSerializer.LoadObjectFromFile(fileName) as CogAcqFifoTool);
                }
                
                  Monitor("相机加载成功……", 3);
                //加载检测vpp

                foreach (string fileName in mCheckPaths )
                {
                    mCheckCCDs.Add(CogSerializer.LoadObjectFromFile(fileName) as CogToolBlock);
                }
                Monitor("检测工具加载成功……", 3);
                //加载标定vpp
                foreach (string fileName in mCalibrationPaths )
                {
                    mCalibrationTools.Add(CogSerializer.LoadObjectFromFile(fileName) as CogToolBlock);
                }
                Monitor("标定工具加载成功……", 3);
             
                InitializePort();//初始化通信 
                this.Invoke(new Action(() =>
                {
                   this.Text = "当前产品：" + mProductName;
                    toolStrip1.Enabled = true;
                }));
            }
            catch (Exception ex)
            {
                Monitor("系统加载异常：" + ex.Message, 3);
                this.Invoke(new Action(() =>
                {
                    toolStrip1.Enabled = true;
                }));

                // MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //保存log日志
        private void SaveLog(object msg)
        {
            String filename = Application.StartupPath + "\\Log\\" + DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                string str = msg.ToString();
                mlogger.Save(str, filename + ".log");
            }
            catch (Exception e)
            {
                mlogger.Save(e.Message, filename + ".log");
            }
        }
        /// <summary>
        /// 面板消息监控
        /// </summary>
        /// <param name="data">要显示的内容</param>
        /// <param name="InOrOut">显示格式:1为接收，2为发送，3为系统显示</param>
        private void Monitor(string data, int InOrOut)
        {
            string TempStr = "";
            string text = string.Empty;
            String filename = Application.StartupPath + "\\Log\\" + DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                if (1 == InOrOut)
                {
                    text = DateTime.Now.ToString() + "接收:" + data + "\r\n" + TempStr;
                }
                else if (2 == InOrOut)
                {
                    text = DateTime.Now.ToString() + "发送:" + data + "\r\n" + TempStr;
                }
                else
                {
                    text = DateTime.Now.ToString() + "显示:" + data + "\r\n" + TempStr;
                }
                this.Invoke(new Action(() =>
                {
                    TextMessage.AppendText(text);
                    TextMessage.ScrollToCaret();

                    TextLine += 1;
                    if (TextLine > 50)
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(SaveLog), TextMessage.Text);
                        TextMessage.Clear();
                        TextLine = 0;
                    }
                }));
            }
            catch (Exception ex)
            {
                text = ex.Message;
                // MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        /// <summary>
        /// 相机1取图设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 相机1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FrmFifoTool frm_fifo = new FrmFifoTool(mAcqCCD1Path);
                frm_fifo.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        /// <summary>
        /// 相机2取图设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 相机2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FrmFifoTool frm_fifo = new FrmFifoTool(mAcqCCD3Path);
                frm_fifo.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        /// <summary>
        /// 关闭软件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                SaveSolution();
                //if (mTcpServer != null)
                //{
                //    mTcpServer.Stop();
                //}
                DispPort();
                CogFrameGrabbers grabs = new CogFrameGrabbers();//释放相机
                foreach (ICogFrameGrabber grab in grabs)
                    grab.Disconnect(true);
            }
            catch (Exception ex)
            {
                Monitor("软件关闭异常:" + ex.Message, 3);
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //  System.Environment.Exit(0);
            }
        }

        /// <summary>
        /// 工位1vpp设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 工位1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCheckTools frmCheck = new FrmCheckTools(mAcqCCD1, mCalibrationTool, mCheckCCD1, mCheckCCD1Path);
                frmCheck.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// 工位2vpp设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 工位2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCheckTools frmCheck = new FrmCheckTools(mAcqCCD3, mCalibrationTool2, mCheckCCD3, mCheckCCD3Path);
                frmCheck.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        /// <summary>
        /// 通信设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                BindPort(false);
                FormPortConfig frm = new FormPortConfig();
                frm.ShowDialog();
                BindPort(true);
            }
            catch (Exception ex)
            {
                Monitor("打开通信面板错误：" + ex.Message, 3);
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void 单步运行ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void LoadImage(CogRecordDisplay HW)
        {
            mProcess = new ImageProcess();

            try
            {
                if (openImage.ShowDialog() == DialogResult.OK)
                {
                  
                    mImageFile.Open(openImage.FileName, CogImageFileModeConstants.Read);
                    mImage = mImageFile[0] as CogImage8Grey;
                    if (mImage != null)
                    {

                        HW.Image = mImage;
                        HW.Fit(true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void 相机ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadImage(hwindows [0]);
            //try
            //{
            //    if (openImage.ShowDialog() == DialogResult.OK)
            //    {
            //        mImageFile.Open(openImage.FileName, CogImageFileModeConstants.Read);
            //        mImage1 = mImageFile[0] as CogImage24PlanarColor;
            //        if (mImage1 != null)
            //        {

            //            hwindow.Image = mImage1;
            //            hwindow.Fit(true);


            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}

        }

        private void 相机2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LoadImage(hwindows [1]);
        }

        private void 相机1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {

                mAcqFifoTools[0].Run();
                if (mAcqFifoTools[0].OutputImage != null)
                {

                    mImage = mAcqFifoTools[0].OutputImage as CogImage8Grey;
                    hwindows[0].Image = mImage;


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void 相机2ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {

                mAcqFifoTools[1].Run();
                if (mAcqFifoTools[1].OutputImage != null)
                {

                    mImage = mAcqFifoTools[1].OutputImage as CogImage8Grey;
                    hwindows[1].Image = mImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void 相机1ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CCD1Work("test", hwindows [0], "model1",null);
        }
        private void fD2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CCD2Work("test", hwindows [1],"model2", null);
        }
        private void 相机2ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            CCD3Work("test", hwindows [2],"model3", null);
        }

        private void FrmMain_SizeChanged(object sender, EventArgs e)
        {
            //  mAsc.controlAutoSize(this);          
        }
        private void tsb_userLogin_Click(object sender, EventArgs e)
        {
            FrmLogin frm = new FrmLogin();
            frm.ShowDialog();
            if (frm.Prefsession == 1)
            {
                tsb_Tool.Enabled = true;
                toolStripButton1.Enabled = true;
                frm.Dispose();
                frm.Close();
            }
        }
        private void 相机3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadImage(hwindows [2]);
        }

        private void fD2ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCheckTools frmCheck = new FrmCheckTools(mAcqCCD2, mCalibrationTool1, mCheckCCD2, mCheckCCD2Path);
                frmCheck.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void button1_Click_1(object sender, EventArgs e)
        {

        }
        // CogRectangle cgRect;
        private void button1_Click_2(object sender, EventArgs e)
        {
            Creatgraphs(hwindow, 1);
        }
        bool Continuous_CCD1;
        bool Continuous_CCD2;
        bool Continuous_CCD3;
        private void fDToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Continuous_CCD1 = !Continuous_CCD1;
            try
            {
                if (Continuous_CCD1 == true)
                {

                    fDToolStripMenuItem2.Text = "关闭实时";

                    hwindow.StartLiveDisplay(mAcqCCD1.Operator);

                    tsb_Tool.Enabled = false;
                    加载图片ToolStripMenuItem.Enabled = false;
                    采集图片ToolStripMenuItem.Enabled = false;
                    单步运行ToolStripMenuItem.Enabled = false;


                    this.Text = "相机1实时采集中！";
                }
                else
                {
                    fDToolStripMenuItem2.Text = "工位1";
                    hwindow.StopLiveDisplay();
                    tsb_Tool.Enabled = true;
                    加载图片ToolStripMenuItem.Enabled = true;
                    采集图片ToolStripMenuItem.Enabled = true;
                    单步运行ToolStripMenuItem.Enabled = true;


                    this.Text = "";
                }

            }
            catch (Exception ex)
            {
                Monitor("实时图像失败:" + ex.Message, 3);
                fDToolStripMenuItem2.Text = "工位1";
                tsb_Tool.Enabled = true;
                加载图片ToolStripMenuItem.Enabled = true;
                采集图片ToolStripMenuItem.Enabled = true;
                单步运行ToolStripMenuItem.Enabled = true;
                this.Text = "";
            }
        }
        private void fD2ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                FrmFifoTool frm_fifo = new FrmFifoTool(mAcqCCD2Path);
                frm_fifo.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void fDToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmCalibration frm = new FrmCalibration(mAcqCCD1, mCalibrationTool, mCalibrationVppPath);
            frm.ShowDialog();
        }
        private void bMToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmCalibration frm = new FrmCalibration(mAcqCCD2, mCalibrationTool1, mCalibrationVppPath1);
            frm.ShowDialog();
        }
        private void cGToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmCalibration frm = new FrmCalibration(mAcqCCD3, mCalibrationTool2, mCalibrationVppPath2);
            frm.ShowDialog();
        }

        private void bMToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Continuous_CCD2 = !Continuous_CCD2;
            try
            {
                if (Continuous_CCD2 == true)
                {

                    fDToolStripMenuItem2.Text = "关闭实时";

                    hwindow1.StartLiveDisplay(mAcqCCD2.Operator);

                    tsb_Tool.Enabled = false;
                    加载图片ToolStripMenuItem.Enabled = false;
                    采集图片ToolStripMenuItem.Enabled = false;
                    单步运行ToolStripMenuItem.Enabled = false;
                    this.Text = "相机2实时采集中！";
                }
                else
                {
                    fDToolStripMenuItem2.Text = "工位2";
                    hwindow1.StopLiveDisplay();
                    tsb_Tool.Enabled = true;
                    加载图片ToolStripMenuItem.Enabled = true;
                    采集图片ToolStripMenuItem.Enabled = true;
                    单步运行ToolStripMenuItem.Enabled = true;
                    this.Text = "";
                }

            }
            catch (Exception ex)
            {
                Monitor("实时图像失败:" + ex.Message, 3);
                fDToolStripMenuItem2.Text = "工位2";
                tsb_Tool.Enabled = true;
                加载图片ToolStripMenuItem.Enabled = true;
                采集图片ToolStripMenuItem.Enabled = true;
                单步运行ToolStripMenuItem.Enabled = true;
                this.Text = "";
            }
        }
        private void cGToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Continuous_CCD3 = !Continuous_CCD3;
            try
            {
                if (Continuous_CCD3 == true)
                {
                    fDToolStripMenuItem2.Text = "关闭实时";
                    hwindow2.StartLiveDisplay(mAcqCCD3.Operator);

                    tsb_Tool.Enabled = false;
                    加载图片ToolStripMenuItem.Enabled = false;
                    采集图片ToolStripMenuItem.Enabled = false;
                    单步运行ToolStripMenuItem.Enabled = false;

                    this.Text = "相机3实时采集中！";
                }
                else
                {
                    fDToolStripMenuItem2.Text = "工位3";
                    hwindow2.StopLiveDisplay();
                    tsb_Tool.Enabled = true;
                    加载图片ToolStripMenuItem.Enabled = true;
                    采集图片ToolStripMenuItem.Enabled = true;
                    单步运行ToolStripMenuItem.Enabled = true;

                    this.Text = "";
                }
            }
            catch (Exception ex)
            {
                Monitor("实时图像失败:" + ex.Message, 3);
                fDToolStripMenuItem2.Text = "工位3";
                tsb_Tool.Enabled = true;
                加载图片ToolStripMenuItem.Enabled = true;
                采集图片ToolStripMenuItem.Enabled = true;
                单步运行ToolStripMenuItem.Enabled = true;
                this.Text = "";
            }
        }
        private void cGToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {

                mAcqFifoTools[2].Run();
                if (mAcqFifoTools[2].OutputImage != null)
                {

                    mImage = mAcqFifoTools[2].OutputImage as CogImage8Grey;
                    hwindows[2].Image = mImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void 旋转中心设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    FrmCalibration frm = new FrmCalibration(mAcqCCD1, mGetRotationTool, mGetRotationVppPath);
        //    frm.ShowDialog();
        }
        void ReceiveData(TcpBase sender, string text)
        {
            this.Invoke(new Action(() =>
            {         
               string str = "";
                if (sender != null)
                {
                    str = "[" + sender.ToString() + "] " + text + "\r\n";
                }
                else
                {
                    str = text + "\r\n";
                }

                Monitor(str, 1);
                // return;
                string workmsg = "";
                mProductCode = "";
                //string product = "";
                if (text != null)
                {
                    string[] data = text.Split(',');
                    workmsg = data[1];                   
                    switch (workmsg)
                    {
                        case "grab1":    // 工位1相机工作
                            CCD1Work(mProductCode, hwindows[0], workmsg, sender);
                            break;
                        case "grab2":    //工位2相机工作
                            //mProductCode = "";
                            CCD2Work(mProductCode, hwindows[1], workmsg, sender);
                            break;
                        case "grab3":    //工位3相机工作

                            CCD3Work(mProductCode, hwindows[2], workmsg, sender);                         
                            break;
                        case "model1":   //获取工位1基准
                            CCD1Work(mProductCode, hwindows[0], workmsg, sender);
                            break;
                        case "model2":   //获取工位2基准
                            CCD2Work(mProductCode, hwindows[1], workmsg, sender);
                            break;
                        case "model3":   //获取工位3基准
                            CCD3Work(mProductCode, hwindows[2], workmsg, sender);
                            break;
                        case "grabcali1":    //CCD1标定工作
                            AutoCalibration(hwindows[0], text);
                            break;
                        case "grabcali2": // CCD2标定工作
                            AutoCalibration(hwindows[1], text);
                            break;                     
                        case "rbot":      //获取机器人旋转中心
                            GetRobotRotation(hwindows[0], text);
                            break;                    
                        default:
                            break;
                    }
                }
            }));
        }
        void ShowMessage(TcpBase sender, string text)
        {
            string str = "";
            if (sender != null)
            {
                str = "[" + sender.ToString() + "] " + text + "\r\n";
            }
            else
            {
                str = text + "\r\n";
            }

            Monitor(str, 3);
        }

        void ConnectStats(TcpBase sender, bool stats)
        {

        }

        private void tsb_Tool_ButtonClick(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                FrmCheckTools fm = new FrmCheckTools(mAcqFifoTools, mAcqPaths, mCheckCCDs, mCheckPaths, mCalibrationTools, mCalibrationPaths);
                fm.ShowDialog();
            });
            
            
            
        }
        bool pr;
        private void toolStripSplitButton2_ButtonClick(object sender, EventArgs e)
        {
            pr = !pr;
            if (pr )
            {
                groupBox1.Visible = true;
                AutoSizeScreen();
            }
            else
            {
                groupBox1.Visible = false ;
                AutoSizeScreen();
            }          
        }
        string productPath; 
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {             
                string productName = this.textBox1.Text;
                string name = textBox1.Text;
                if (name != "")
                {
                    treeView_product.Nodes[0].Nodes.Add(name);
                    productPath = SolutionPath + "\\" + productName; //以当前产品名创建文件夹
                    if (!Directory.Exists(productPath))                  
                     Directory.CreateDirectory(productPath);                  
                }
                else
                {
                    MessageBox.Show("产品名为空，请重命名！");
                    return;
                }             
                ///将当前vpp复制过去
                copyVpp(LoadPath, productPath);
                //更新vpp
                UpdateVpp(productPath);              
                MessageBox.Show("产品增加成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show("产品增加失败:" + ex.Message);
            }            
        }
        string strpath = Application.StartupPath + "\\Connetconfig.ini";
        private void button4_Click(object sender, EventArgs e)
        {            
            string productName = strName;
            this.Text = "当前产品:" + strName;
            mProductName = strName;
            productNum = Convert.ToInt32(mProductName.Substring(mProductName.Length - 1, 1));
            productPath = SolutionPath + "\\" + productName; //以当前产品名创建文件夹           
            UpdateVpp(productPath);        
            mOperation.WirteIni("方案设置", "当前产品", mProductName, strpath);
            ThreadPool.QueueUserWorkItem(new WaitCallback(LoadVpp), productPath); //初始化vpp     
        }
        string xmlPath = Application.StartupPath + "\\Procuctconfig.xml";
        XmlTool mXlm = new XmlTool();
        private void button2_Click(object sender, EventArgs e)
        {

            string moveName = strName;
            int node = treeView_product.SelectedNode.Index;
           if (moveName ==mProductName )
           {
               MessageBox.Show("需要移除的产品为当前加载产品！请重新选择");
               return;
           }
            try
            {
                productPath = SolutionPath + "\\" + moveName; //以当前产品名创建文件夹
                if (Directory.Exists(productPath)) 
                {
                    Directory.Delete(productPath, true);//不经过回收站，直接删除
                }
                treeView_product.Nodes[0].Nodes.RemoveAt(node);//删除treeview数据
                MessageBox.Show("产品删除成功!");

            }
            catch (Exception ex)
            {
                MessageBox.Show("产品删除失败:" + ex.Message);
            }
                       
        }
        string strName;
        private void treeView_product_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            strName = e.Node.Text;
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {              
            
               mXlm.TreeViewToXml(treeView_product, xmlPath);
               MessageBox.Show("保存成功！");
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存失败：" + ex.Message);
            }
               
        }
        private void toolStripSplitButton3_ButtonClick(object sender, EventArgs e)
        {
            FrmParamData frm = new FrmParamData(productPath);
          //  FrmData = new FrmParamData(productPath);
            frm.PushData += new FrmParamData.ClickEventHandler(ReceiveParamMsg);
            frm.ShowDialog ();
        }

        private void 加载图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 工位1ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CCD1Work("test", hwindows[0], "123", null);
        }

        private void 工位2ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CCD2Work("test", hwindows[1], "123", null);
        }

        private void 工位3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CCD3Work("test", hwindows[2], "123", null);
        }
    }
}
